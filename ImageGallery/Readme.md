ImageGallery:

I didn't have time to finish all I wanted so there is some information about the code.

Environment: solution we use in the current project. Allows us to easily prepare separate environments for testing, etc.

Service: very basic implementation to have one place specialized in network calls. In this case, also I use it to cache images.

ConnectivityMonitor: a simple class that allows me to have one place to check if there is an internet connection. It is also published value so we can observe it but had no time to actually use it properly.

ImageCollection: the main view that shows us all the images. There is an error when there is no internet connection but you need to restart the app to load images again but with more time I would prepare a solution to drag collection down to refresh or just observe the connectivity monitor to start it automatically. And I am aware that there should be also some kind of check if it is the last page etc. right now it loads the next pages infinitely but again it is because of a time.

ImageDetails: this view is very basic I didn't prepare any change in UI when it changes orientation but there is a working share button and it downloads a full-size image instead of a small thumbnail like in the previous view.

A lot of those things are not completely finished but I preferred to do this like that to show I know some of the concepts etc. but I had not enough time to finish everything I wanted.
