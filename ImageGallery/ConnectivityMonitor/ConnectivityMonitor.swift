//
//  ConnectivityMonitor.swift
//  ImageGallery
//
//  Created by Bartosz Gola on 11/12/2021.
//

import Foundation
import Network
import Combine

protocol ConnectivityMonitorInterface {
    var isAvailable: Bool { get }
}

class ConnectivityMonitor: ConnectivityMonitorInterface {
    static let monitorQueue = DispatchQueue(label: "ConnectivityMonitor")
    
    @Published private(set) var isAvailable: Bool
    private let monitor: NWPathMonitor

    init(monitor: NWPathMonitor = NWPathMonitor()) {
        self.monitor = monitor
        isAvailable = monitor.currentPath.status == .satisfied
        configureMonitor()
    }

    private func configureMonitor() {
        monitor.pathUpdateHandler = { [weak self] path in
            self?.isAvailable = path.status == .satisfied
        }
        monitor.start(queue: ConnectivityMonitor.monitorQueue)
    }
}
