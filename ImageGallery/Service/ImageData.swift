//
//  ImageData.swift
//  ImageGallery
//
//  Created by Bartosz Gola on 11/12/2021.
//

import Foundation

struct ImageData: Decodable {
    let id: String
    let author: String
    let width: Int
    let height: Int
    let url: String
    let download_url: String
}
