//
//  Service.swift
//  ImageGallery
//
//  Created by Bartosz Gola on 11/12/2021.
//

import Foundation
import UIKit
import Network

enum ServiceError: Error {
    case creatingUrlFromString
    case decodingImageFromData
}

protocol ServiceInteface {
    typealias ImageLoadingCompletion = (urlString: String, result: Result<UIImage, Error>)
    
    func loadImage(urlString: String, completion: @escaping (ImageLoadingCompletion) -> Void)
    func call(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void)
}

class Service: ServiceInteface {
    private let imageCache = NSCache<NSString, UIImage>()
    private let session = URLSession(configuration: .default)
    
    func loadImage(urlString: String, completion: @escaping (ImageLoadingCompletion) -> Void) {
        if let cachedImage = imageCache.object(forKey: NSString(string: urlString)) {
            completion((urlString: urlString, result: .success(cachedImage)))
            return
        }
        
        //TODO: Download and store image in cashe
        guard let url = URL(string: urlString) else {
            completion((urlString: urlString, result: .failure(ServiceError.creatingUrlFromString)))
            return
        }
        
        call(url: url) { [weak self] data, _, error in
            if let error = error {
                completion((urlString: urlString, result: .failure(error)))
                return
            }
            
            guard let data = data,
                  let image = UIImage(data: data) else {
                completion((urlString: urlString, result: .failure(ServiceError.decodingImageFromData)))
                return
            }
            self?.imageCache.setObject(image, forKey: NSString(string: urlString))
            completion((urlString: urlString, result: .success(image)))
        }
    }
    
    func call(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        session.dataTask(with: url, completionHandler: completion).resume()
    }
}
