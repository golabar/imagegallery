//
//  ImageDetailViewController.swift
//  ImageGallery
//
//  Created by Bartosz Gola on 12/12/2021.
//

import Foundation
import UIKit

class ImageDetailViewController: UIViewController {
    private let activityIndicator = UIActivityIndicatorView()
    private let imageView = UIImageView()
    private let authorLabel = UILabel()
    private let shareButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "square.and.arrow.up"), for: .normal)
        return button
    }()
    private let errorLabel: UILabel = {
        let label = UILabel()
        label.textColor = .red
        label.text = "Error!"
        return label
    }()
    
    private let viewModel: ImageDetailViewModel
    
    init(viewModel: ImageDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareViews()
        initialSetup()
        setupValues()

        shareButton.addTarget(self, action: #selector(shareButtonTapped), for: .touchUpInside)
    }
    
    private func setupValues() {
        authorLabel.text = viewModel.getAuthor()
        viewModel.loadImage() { result in
            DispatchQueue.main.async { [weak self] in
                self?.activityIndicator.stopAnimating()
                self?.activityIndicator.isHidden = true
                
                switch result {
                case let .success(image):
                    self?.imageView.isHidden = false
                    self?.imageView.image = image
                    self?.shareButton.isEnabled = true
                case .failure(_):
                    self?.errorLabel.isHidden = false
                }
            }
        }
    }
    
    private func initialSetup() {
        shareButton.isEnabled = false
        errorLabel.isHidden = true
        imageView.isHidden = true
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
    private func prepareViews() {
        view.backgroundColor = .white
        
        view.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        imageView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        
        view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerXAnchor.constraint(equalTo: imageView.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: imageView.centerYAnchor).isActive = true
        
        view.addSubview(shareButton)
        shareButton.translatesAutoresizingMaskIntoConstraints = false
        shareButton.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 16).isActive = true
        shareButton.rightAnchor.constraint(equalTo: imageView.rightAnchor, constant: -16).isActive = true
        
        view.addSubview(errorLabel)
        errorLabel.translatesAutoresizingMaskIntoConstraints = false
        errorLabel.centerXAnchor.constraint(equalTo: imageView.centerXAnchor).isActive = true
        errorLabel.centerYAnchor.constraint(equalTo: imageView.centerYAnchor).isActive = true
        
        view.addSubview(authorLabel)
        authorLabel.translatesAutoresizingMaskIntoConstraints = false
        authorLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        authorLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 32).isActive = true
        
        view.bringSubviewToFront(shareButton)
        view.bringSubviewToFront(errorLabel)
        view.bringSubviewToFront(activityIndicator)
    }
    
    @objc private func shareButtonTapped() {
        guard let imageUrl = imageView.image else { return }
        let activityViewController = UIActivityViewController(activityItems: [imageUrl] as [Any],
                                                              applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = shareButton
        present(activityViewController, animated: true, completion: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
