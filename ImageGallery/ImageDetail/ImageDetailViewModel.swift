//
//  ImageDetailViewModel.swift
//  ImageGallery
//
//  Created by Bartosz Gola on 12/12/2021.
//

import Foundation
import UIKit

class ImageDetailViewModel {
    let imageData: ImageData
    
    init(imageData: ImageData) {
        self.imageData = imageData
    }
    
    func loadImage(completion: @escaping (Result<UIImage, Error>) -> Void) {
        Environment.current.service.loadImage(urlString: imageData.download_url) { _, result in
            completion(result)
        }
    }
    
    func getAuthor() -> String {
        imageData.author
    }
}
