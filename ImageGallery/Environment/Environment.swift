//
//  Environment.swift
//  ImageGallery
//
//  Created by Bartosz Gola on 11/12/2021.
//

import Foundation

protocol EnvironmentInterface {
    var connectivityMonitor: ConnectivityMonitorInterface { get }
    var service: ServiceInteface { get }
}

class Environment: EnvironmentInterface {
    static var current: EnvironmentInterface = Environment.prepareDefaultEnviroment()
    
    let connectivityMonitor: ConnectivityMonitorInterface
    let service: ServiceInteface
    
    init(connectivityMonitor: ConnectivityMonitorInterface,
                service: ServiceInteface) {
        self.connectivityMonitor = connectivityMonitor
        self.service = service
    }
    
    static func prepareDefaultEnviroment() -> EnvironmentInterface {
        let connectivityMonitor = ConnectivityMonitor()
        let service = Service()
        
        return Environment(connectivityMonitor: connectivityMonitor,
                           service: service)
    }
}
