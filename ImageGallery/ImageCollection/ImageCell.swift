//
//  ImageCell.swift
//  ImageGallery
//
//  Created by Bartosz Gola on 12/12/2021.
//

import Foundation
import UIKit

class ImageCell: UICollectionViewCell {
    private let activityIndicator = UIActivityIndicatorView()
    private let imageView = UIImageView()
    private let errorLabel: UILabel = {
        let label = UILabel()
        label.textColor = .red
        label.text = "Error!"
        return label
    }()
    
    private var imageUrl = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepareViews()
        initialSetup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        initialSetup()
    }
    
    public func setupCell(imageUrlString string: String) {
        imageUrl = string
        Environment.current.service.loadImage(urlString: string) { [weak self] urlString, result in
            guard let self = self,
                  urlString == self.imageUrl else { return }
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                
                switch result {
                case let .success(image):
                    self.imageView.isHidden = false
                    self.imageView.image = image
                case .failure(_):
                    self.errorLabel.isHidden = false
                }
            }
        }
    }
    
    private func initialSetup() {
        imageView.image = nil
        errorLabel.isHidden = true
        imageView.isHidden = true
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
    private func prepareViews() {
        addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        addSubview(errorLabel)
        errorLabel.translatesAutoresizingMaskIntoConstraints = false
        errorLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        errorLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        imageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        bringSubviewToFront(errorLabel)
        bringSubviewToFront(activityIndicator)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
