//
//  ImageCollectionViewModel.swift
//  ImageGallery
//
//  Created by Bartosz Gola on 12/12/2021.
//

import Foundation
import UIKit

class ImageCollectionViewModel {
    @Published private(set) var dataSource = [ImageData]()
    @Published var isPortait = UIApplication.shared.windows.first?.windowScene?.interfaceOrientation.isPortrait ?? true
    
    private var nextPage = 0
    private var currentPage: Int? = nil
    private let baseUrl = "https://picsum.photos/"
    private let listString = "v2/list"
    private let thumbnailSize = 200
    
    func loadPage(completion: (() -> Void)? = nil) {
        let urlString = "\(baseUrl)\(listString)?page=\(nextPage)"
        guard currentPage != nextPage,
              let url = URL(string: urlString) else { return }
        
        currentPage = nextPage
        Environment.current.service.call(url: url) { [weak self] data, _, _ in
            guard let data = data,
            let newImageDatas = try? JSONDecoder().decode([ImageData].self, from: data) else { return }
            self?.nextPage += 1
            self?.dataSource.append(contentsOf: newImageDatas)
            completion?()
        }
    }
    
    func getThumbnailImageUrl(for index: Int) -> String {
        guard index < dataSource.count && index >= 0 else { return "" }
        let id = dataSource[index].id
        return "\(baseUrl)id/\(id)/\(thumbnailSize)"
    }
    
    func getItemSize(viewWidth: CGFloat) -> CGSize {
        var numberOfItems: CGFloat = 1
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            numberOfItems = isPortait ? 5 : 10
        } else {
            numberOfItems = isPortait ? 2 : 4
        }
        
        let dimension = viewWidth / numberOfItems
        return CGSize(width: dimension, height: dimension)
    }
    
    func checkInternetConnection() -> Bool {
        Environment.current.connectivityMonitor.isAvailable
    }
}
