//
//  ImageCollectionViewController+CollectionView.swift
//  ImageGallery
//
//  Created by Bartosz Gola on 12/12/2021.
//

import Foundation
import UIKit

extension ImageCollectionViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let imageCell: ImageCell!
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: imageCellIdentifier, for: indexPath) as? ImageCell
        
        if let unwrappedCell = cell {
            imageCell = unwrappedCell
        } else {
            imageCell = ImageCell()
        }
        
        imageCell.setupCell(imageUrlString: viewModel.getThumbnailImageUrl(for: indexPath.row))
        
        return imageCell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard indexPath.row == (viewModel.dataSource.count - 10) else { return }
        viewModel.loadPage()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        
        let imageDetailVM = ImageDetailViewModel(imageData: viewModel.dataSource[indexPath.row])
        let imageDetailVC = ImageDetailViewController(viewModel: imageDetailVM)
        
        present(imageDetailVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        viewModel.getItemSize(viewWidth: view.bounds.width)
    }
}
