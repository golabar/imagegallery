//
//  ImageCollectionViewController.swift
//  ImageGallery
//
//  Created by Bartosz Gola on 11/12/2021.
//

import UIKit
import Combine

class ImageCollectionViewController: UIViewController {

    private let activityIndicator = UIActivityIndicatorView()
    private var collectionView: UICollectionView!
    private var cancellables = Set<AnyCancellable>()
    
    let imageCellIdentifier = "ImageCell"
    let viewModel = ImageCollectionViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        prepareViews()
        initialSetup()
        bindData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard viewModel.checkInternetConnection() else {
            showNoInternetConnactionAlert()
            return
        }
        loadFirstPage()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        viewModel.isPortait = size.width < size.height
    }
    
    private func initialSetup() {
        collectionView.isHidden = true
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
    private func loadFirstPage() {
        viewModel.loadPage() {
            DispatchQueue.main.async { [weak self] in
                self?.activityIndicator.stopAnimating()
                self?.activityIndicator.isHidden = true
                self?.collectionView.isHidden = false
            }
        }
    }
    
    private func bindData() {
        viewModel.$dataSource
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.collectionView.reloadData()
            }.store(in: &cancellables)
        viewModel.$isPortait
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.collectionView.reloadData()
            }.store(in: &cancellables)
    }
    
    private func configureCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(ImageCell.self, forCellWithReuseIdentifier: imageCellIdentifier)
        collectionView.showsVerticalScrollIndicator = false
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    private func showNoInternetConnactionAlert() {
        let alert = UIAlertController(title: "No internet connection",
                                      message: "Please connect to the Internet and restart app.",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true)
    }
    
    private func prepareViews() {
        view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        
        view.bringSubviewToFront(activityIndicator)
    }
}

